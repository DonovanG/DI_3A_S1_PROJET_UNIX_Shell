#include "shell_fct.h"
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>

int exec_command(cmd* my_cmd){
	if(my_cmd->nbCmdMembers!=0){
        //redirection directory system
		if (strcmp(my_cmd->cmdMembersArgs[0][0],"cd")==0){
			if(my_cmd->cmdMembersArgs[0][1]==NULL)
				return chdir(getenv("HOME")); //Direction vers la variable env HOME
			else
				return chdir(my_cmd->cmdMembersArgs[0][1]);//Direction vers le/s dossiers
		}
		//variables
        pid_t tab_pid[my_cmd->nbCmdMembers];
        int status_pid[my_cmd->nbCmdMembers];
        int tab_tube[my_cmd->nbCmdMembers][2];
        //exec command
        for(int i=0;i<my_cmd->nbCmdMembers;i++){
			//Watch dog init
			void arretFils(int sigNum){
				fflush(stdout);
				close(tab_tube[i][0]);
			}
            //Open pipe
            pipe(tab_tube[i]);
			//creation du process
            tab_pid[i] = fork();
            //child process
            if(tab_pid[i] == 0){
				//Appel du chien de garde
                signal(SIGALRM, arretFils);
				//attente de 5 seconde				
				alarm(5);
				//pour tout sauf le premier
				if(i>0){
                    close(tab_tube[i-1][1]); //Close std output tube-1
                    //Redirect child process input and ouputs
                    dup2(tab_tube[i-1][0], 0);
                    close(tab_tube[i-1][0]); //Close std input tube-1
                }
                //Redirect child process input and ouputs
                close(tab_tube[i][0]);  //Close std input
                if(i!=my_cmd->nbCmdMembers-1)
					dup2(tab_tube[i][1], 1);//Dup pipe output to std output
                close(tab_tube[i][1]);  //Close pipe output
                
				//REDIRECTION
				//Redicection en entree std
                if(my_cmd->redirection[i][0]!=NULL){
                    //ouverture descr fichier
                    int fd;
					if((fd=open(my_cmd->redirection[i][0], O_RDWR, S_IRWXU|S_IRWXG|S_IRWXO))==-1){
						perror("open");
                       	exit(errno);
					}
                    //Redirection en entree std
                    if(dup2(fd, 0)==-1){
                        perror("dup2");
                        exit(errno);
                    };
                    close(fd);
                }
				//Redicection en sortie std
                if(my_cmd->redirection[i][1]!=NULL){
					int fd;
                    //APPEND or OVERWRITE
					if(my_cmd->redirectionType[i][1]==1){
						if((fd=open(my_cmd->redirection[i][1], O_RDWR|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO))==-1){
							perror("open");
                        	exit(errno);
						}
					}else{
						if((fd=open(my_cmd->redirection[i][1], O_RDWR|O_CREAT|O_TRUNC, S_IRWXU|S_IRWXG|S_IRWXO))==-1){
							perror("open");
                        	exit(errno);
						}
					}
					//on se place a la fin du fichier
					lseek(fd, 0, SEEK_END);
					
                    //Redirection en entree std
                    if(dup2(fd, 1)==-1){
                        perror("dup2");
                        exit(errno);
                    };
					//fermeture du fichier
                    close(fd);
                }
				//Redicection en sortie d'erreur
                if(my_cmd->redirection[i][2]!=NULL){
					//ouverture du fichier
                    int fd;
					//APPEND or OVERWRITE
					if(my_cmd->redirectionType[i][2]==1){
						if((fd=open(my_cmd->redirection[i][2], O_RDWR|O_CREAT, S_IRWXU|S_IRWXG|S_IRWXO))==-1){
							perror("open");
                        	exit(errno);
						}
					}else{
						if((fd=open(my_cmd->redirection[i][2], O_RDWR|O_CREAT|O_TRUNC, S_IRWXU|S_IRWXG|S_IRWXO))==-1){
							perror("open");
                        	exit(errno);
						}
					}
					//on se place a la fin du fichier
					lseek(fd, 0, SEEK_END);
                    //Redirection en entree std
                    if(dup2(fd, 2)==-1){
                        perror("dup2");
                        exit(errno);
                    };
					//fermeture du fichier
                    close(fd);
                }
		        //exec command
		        if(execvp(my_cmd->cmdMembersArgs[i][0], my_cmd->cmdMembersArgs[i]) == -1) {
		            perror("execvp");
		            exit(errno);
		        }
            }
			//parent process
            if(i>0){
				close(tab_tube[i-1][1]);//Close child output tube-1
				close(tab_tube[i-1][0]);//Close child input tube-1
			}
        }
		close(tab_tube[my_cmd->nbCmdMembers-1][1]);//Close child output last tube
		close(tab_tube[my_cmd->nbCmdMembers-1][0]);//Close child input last tube
        //On verifie que tout les enfants sont termine
		for(int i=0;i<my_cmd->nbCmdMembers;i++)
            waitpid(tab_pid[i],&status_pid[i],0);
	}
    return 0; //return 0
}
