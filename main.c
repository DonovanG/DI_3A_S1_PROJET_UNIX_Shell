#include <pwd.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "shell_fct.h"


//To complete
int main(int argc, char** argv)
{
	//Variables
	int ret = MYSHELL_CMD_OK;
	char* readlineptr = NULL;
	struct passwd* infos;
	char str[1024];
	char hostname[256];
	char workingdirectory[256];
	cmd * commande = NULL;
	//start using history
	using_history();
	//Command call
	while(ret != MYSHELL_FCT_EXIT)
	{
		//Get your session info
		infos=getpwuid(getuid());
		gethostname(hostname, 256);
		getcwd(workingdirectory, 256);
		//Print it to the console
		sprintf(str, "\n{myshell}%s@%s:%s$ ", infos->pw_name, hostname, workingdirectory);
		readlineptr = readline(str);
		//exit the program		
		if (strstr(readlineptr, "exit") != NULL) {
			ret = MYSHELL_FCT_EXIT;
		}else{		
			//allocate memory for comand
			add_history(readlineptr);
			commande = (cmd*)malloc(sizeof(cmd));
			cmd_init(commande,readlineptr);
			//parse the comand
			parseMembers(readlineptr,commande);
			//print the comand
			//printCmd(commande);
			//Execute the comand			
			exec_command(commande);
			//Clean the house
			freeCmd(commande);
		}
		//free readline
		if(readlineptr!=NULL)free(readlineptr);
	}
	//clear history before exit
	clear_history();
	//..........
	return 0;
}
