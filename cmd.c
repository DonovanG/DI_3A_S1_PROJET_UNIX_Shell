#include "cmd.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

//Prints the command
void printCmd(cmd *cmd){
    printf("init_cmd=%s\n",cmd->initCmd);
    //print the number of member
    if(cmd->nbCmdMembers) printf("nb_cmd_members=%d\n", cmd->nbCmdMembers);
    //print all the member
    for(int i = 0; i<cmd->nbCmdMembers; i++)
        if(cmd->cmdMembers[i]) printf("cmd_members[%d]=%s\n", i, cmd->cmdMembers[i]);
    //print each arguments for each members
    for(int k = 0; k<cmd->nbCmdMembers; k++){
        for(int j = 0; j<cmd->nbMembersArgs[k]; j++){
            if(cmd->cmdMembersArgs[k][j]) printf("cmd_members_args[%d][%d]=%s\n", k, j, cmd->cmdMembersArgs[k][j]);
        }
    }
    //print the number of arguments per member
    for(int x = 0; x<cmd->nbCmdMembers; x++)
        if(cmd->nbMembersArgs[x]) printf("nb_members_args[%d]=%d\n", x, cmd->nbMembersArgs[x]);
    //print the different redirection for each member
    for(int z = 0; z<cmd->nbCmdMembers; z++){
        if(cmd->redirection[z][0]) printf("redirection[%d][STDIN]=%s\n", z, cmd->redirection[z][0]);
        if(cmd->redirection[z][1]) printf("redirection[%d][STDOUT]=%s\n", z, cmd->redirection[z][1]);
        if(cmd->redirection[z][2]) printf("redirection[%d][STDERR]=%s\n", z, cmd->redirection[z][2]);
    }
    //print the type of redirection for each member
    for(int z = 0; z<cmd->nbCmdMembers; z++){
        if(cmd->redirectionType[z][1]==1)
			printf("redirectionType[%d][STDOUT]=APPEND\n", z);
		if(cmd->redirectionType[z][1]==2)
			printf("redirectionType[%d][STDOUT]=OVERWRITE\n", z);
		if(cmd->redirectionType[z][2]==1)
			printf("redirectionType[%d][STDERR]=APPEND\n", z);
		if(cmd->redirectionType[z][2]==2)
			printf("redirectionType[%d][STDERR]=OVERWRITE\n", z);
	}
}

//init the comand
void cmd_init(cmd* cmd, char * str){
	cmd->initCmd = NULL;
	if(str!=NULL)
		cmd->initCmd = strdup(str);
	cmd->nbCmdMembers = 0;
	cmd->cmdMembers = NULL;
	cmd->cmdMembersArgs = NULL;
	cmd->nbMembersArgs = NULL;
	cmd->redirection = NULL;
	cmd->redirectionType = NULL;
}

//allocate memory and complete the comand when we knoow the number of members
void cmd_complete(cmd* commande, int comptMemb){
    if(comptMemb > 0){
	    commande->cmdMembersArgs = (char***)malloc((size_t)(comptMemb)*sizeof(char**));
	    commande->nbMembersArgs = (unsigned int *)malloc((size_t)(comptMemb)*sizeof(unsigned int));
	    commande->redirection = (char***)malloc((size_t)(comptMemb)*sizeof(char**));
	    commande->redirectionType = (int**)malloc((size_t)(comptMemb)*sizeof(int*));


	    for(int i = 0; i<comptMemb; i++){
			commande->cmdMembersArgs[i] = NULL;
			commande->nbMembersArgs[i] = 0;
			commande->redirection[i] = NULL;
			commande->redirectionType[i] = NULL;

		commande->redirection[i] = (char**)malloc(3*sizeof(char*));
		commande->redirectionType[i] = (int*)malloc(3*sizeof(int));

		for(int j=0; j<3; j++){
		    commande->redirection[i][j] = NULL;
		    commande->redirectionType[i][j] = 0;
		}
	    }
    }
    commande->nbCmdMembers = comptMemb;

}

//Initializes the initial_cmd, membres_cmd et nb_membres fields
void parseMembers(char *inputString,cmd * commande){

	//define the delimiter for pipes
	const char pipe[2] = "|";
	//define the delimiter for arguments
	const char espace[2] = " ";
	//local variables
	char * str = NULL;
    
	str = strdup(inputString);

	char * str2 = NULL;
	char * token = NULL;
	char * token2 = NULL;
	int comptMemb = 0;
	int comptArg = 0;
	int redirection = 0;

	/* 	strtok replace each delimiter "|" in str by "/0" so the string is cut in substring
	*	token is the first substring of str because it end by "\0"
	*/
	token = strtok(str, pipe);

	/* walk through substrings or tokens */
	while(token != NULL ) {
	    /* allocate memory for the new member */
	    if(commande->cmdMembers != NULL)
		commande->cmdMembers = (char**)realloc(commande->cmdMembers,(size_t)(comptMemb+1)*sizeof(char*));
	    else
		commande->cmdMembers = (char**)malloc((size_t)(sizeof(char*)));
	    /* define the new member */
	commande->cmdMembers[comptMemb] = strdup(token);
	/* check the next substring */
	token = strtok(NULL, pipe);
	comptMemb++;
	}

	/* complete comand and allocate memory */
	cmd_complete(commande, comptMemb);

	/* cut each member to define the arguments */
	for(int j = 0; j<comptMemb; j++){
	/* init number of arguments */
	   comptArg = 0;
	   redirection = 0;
	if(commande->cmdMembers[j] != NULL)
		str2 = strdup(commande->cmdMembers[j]);
	   /* parse the member with " " to find arguments, token 2 = first substring so the first argument */

	token2 = strtok(str2, espace);
	   /* look for each arguments */
	   while(token2 != NULL){
	       /* redirection allow to know if we find a "<, <<, >> or >" before and put the current substring where it belong */
			if(redirection==0){
				/* if we find a "<" we put redirection to 1 to memorise that its an entry redirection */
				if(strcmp(token2,"<")==0){
					token2 = strtok(NULL, espace);
					commande->redirectionType[j][0]= 1;
					commande->redirection[j][0]= strdup(token2);
					token2 = strtok(NULL, espace);
				}
				if(token2 != NULL){
					/* if we find a ">" we put redirection to 2 to memorise that its an exit redirection */
					if(strcmp(token2,">")==0){
						token2 = strtok(NULL, espace);
						commande->redirectionType[j][1]= 2;
						commande->redirection[j][1]= strdup(token2);
						token2 = strtok(NULL, espace);
					}
						/* if we find a ">" we put redirection to 2 to memorise that its an exit redirection */
					else if(strcmp(token2,">>")==0){
						token2 = strtok(NULL, espace);
						commande->redirectionType[j][1]= 1;
						commande->redirection[j][1]= strdup(token2);
						token2 = strtok(NULL, espace);
					}
					if(token2 != NULL){
						/* if we find a ">" we put redirection to 2 to memorise that its an exit redirection */
						if(strcmp(token2,"2>")==0){
							token2 = strtok(NULL, espace);
							commande->redirectionType[j][2]= 2;
							commande->redirection[j][2]= strdup(token2);
							token2 = strtok(NULL, espace);
						}
							/* if we find a ">" we put redirection to 2 to memorise that its an exit redirection */
						else if(strcmp(token2,"2>>")==0){
							token2 = strtok(NULL, espace);
							commande->redirectionType[j][2]= 1;
							commande->redirection[j][2]= strdup(token2);
							token2 = strtok(NULL, espace);
						}
						else{
							if(commande->cmdMembersArgs[j]!=NULL)
								commande->cmdMembersArgs[j] = (char**)realloc(commande->cmdMembersArgs[j],(size_t)(comptArg+2)*sizeof(char*));  
							else
								commande->cmdMembersArgs[j] = (char**)malloc(2*sizeof(char*));
							/* define the new argument */
							commande->cmdMembersArgs[j][comptArg] = strdup(token2);
							commande->cmdMembersArgs[j][comptArg+1]=NULL;
							token2 = strtok(NULL, espace);
							/* increase the number of arguments */
							comptArg++;
						}
					}
				}
	    	}

		}
		/* define the number of arguments */
		commande->nbMembersArgs[j] = comptArg;
		/* free local variables */
		if(str2 != NULL) free(str2);
	}
	if(str != NULL) free(str);
}

//Frees memory associated to a cmd
void freeCmd(cmd  * cmd){
    if(cmd->initCmd != NULL) free(cmd->initCmd);

    for(int i = 0; i<cmd->nbCmdMembers; i++){
    	if(cmd->cmdMembers[i]!=NULL) free(cmd->cmdMembers[i]);
    }
    if(cmd->cmdMembers != NULL) free(cmd->cmdMembers);

    for(int i = 0; i<cmd->nbCmdMembers; i++){
	for(int j = 0; j<cmd->nbMembersArgs[i]; j++){
	        if(cmd->cmdMembersArgs[i][j] != NULL) free(cmd->cmdMembersArgs[i][j]);
	}
    }

    for(int i = 0; i<cmd->nbCmdMembers; i++){
        if(cmd->cmdMembersArgs[i] != NULL) free(cmd->cmdMembersArgs[i]);
    }

    if(cmd->cmdMembersArgs != NULL) free(cmd->cmdMembersArgs);

    if(cmd->nbMembersArgs != NULL) free(cmd->nbMembersArgs);

    for(int i = 0; i<cmd->nbCmdMembers; i++){
        if(cmd->redirection[i][0] != NULL) free(cmd->redirection[i][0]);
		if(cmd->redirection[i][1] != NULL) free(cmd->redirection[i][1]);
		if(cmd->redirection[i][2] != NULL) free(cmd->redirection[i][2]);
		if(cmd->redirection[i] != NULL)free(cmd->redirection[i]);
    }
    if(cmd->redirection != NULL) free(cmd->redirection);

    for(int i = 0; i<cmd->nbCmdMembers; i++){
        if(cmd->redirectionType[i] != NULL) free(cmd->redirectionType[i]);
    }
    if(cmd->redirectionType != NULL) free(cmd->redirectionType);

    if(cmd != NULL) free(cmd);

}
